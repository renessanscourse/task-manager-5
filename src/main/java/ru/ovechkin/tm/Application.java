package ru.ovechkin.tm;

import ru.ovechkin.tm.constant.TerminalConst;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        parseArgs(args);
    }

    private static void displayWelcome() {
        System.out.println();
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + "\t- Show developer info");
        System.out.println(TerminalConst.VERSION + "\t- Show version info");
        System.out.println(TerminalConst.HELP + "\t- Display terminal commands");
    }

}
